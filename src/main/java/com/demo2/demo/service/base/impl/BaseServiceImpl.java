package com.demo2.demo.service.base.impl;

import com.demo2.demo.model.base.BaseIdModel;
import com.demo2.demo.service.base.BaseService;
import com.inker.grpc.GetCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.cassandra.core.query.CassandraPageRequest;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.domain.Slice;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;
import java.util.List;

@Slf4j
public class BaseServiceImpl<T extends BaseIdModel, ID extends UUID> implements BaseService<T, ID> {
    private final CassandraRepository<T, ID> tidCassandraRepository;
    private final Class<T> type;

    public BaseServiceImpl(CassandraRepository<T, ID> tidCassandraRepository, Class<T> type) {
        this.tidCassandraRepository = tidCassandraRepository;
        this.type = type;
    }

    @Override
    public T create(T object) {
        log.info("Crate: " + object);
        object.setId(UUID.randomUUID());
        object.setCreatedDate(LocalDateTime.now());
        object.setCreatedBy("LEGACY");
        return this.tidCassandraRepository.save(object);
    }

    @Override
    public List<T> createAll(List<T> objects) {
        log.info("CrateAll: " + objects);
        objects.forEach(object -> {
            object.setId(UUID.randomUUID());
            object.setCreatedDate(LocalDateTime.now());
            object.setCreatedBy("LEGACY");
        });
        log.info("CrateAll forEach: " + objects);
        return this.tidCassandraRepository.saveAll(objects);
    }

    @Override
    public T update(T object, ID id) {
        Optional<T> toUpdateObject = this.tidCassandraRepository.findById(id);
        return null;
    }


    // TODO: HACER FUNCIONAR LA PAGINACION CON OFFSET EN CASSANDRA
    //  https://docs.datastax.com/en/developer/java-driver/3.2/manual/paging/#offset-queries
    @Override
    public Slice<T> findAll(GetCommand getCommand) {
        String command = getCommand.getCommand().getValueDescriptor().getName();
        CassandraPageRequest pageable;
        switch (command) {
            case "GET_PARTIAL":
                pageable = CassandraPageRequest.of(0, getCommand.getLimit());
                break;
            case "GET_ALL":
                pageable = CassandraPageRequest.of(0, 1000);
                break;
            default:
                pageable = CassandraPageRequest.of(0, 2);
        }
        return this.tidCassandraRepository.findAll(pageable);
    }

    @Override
    public Optional<T> findById(ID id) {
        return this.tidCassandraRepository.findById(id);
    }

    @Override
    public T remove(ID id) {
        T model = this.tidCassandraRepository.findById(id)
                .orElse(null);
        if (model == null) {
            log.error("User with id " + id + " not found");
        }
        this.tidCassandraRepository.deleteById(id);
        return model;
    }
}
