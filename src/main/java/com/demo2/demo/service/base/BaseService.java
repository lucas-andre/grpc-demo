package com.demo2.demo.service.base;

import com.demo2.demo.model.base.BaseIdModel;
import com.inker.grpc.GetCommand;
import org.springframework.data.domain.Slice;

import java.util.Optional;
import java.util.UUID;
import java.util.List;

public interface BaseService<T extends BaseIdModel, ID extends UUID> {
    T create(T object);

    List<T> createAll(List<T> objects);

    T update(T object, ID id);

    Slice<T> findAll(GetCommand command);

    Optional<T> findById(ID id);

    T remove(ID id);
}
