package com.demo2.demo.service.impl;

import com.demo2.demo.model.domain.UserModel;
import com.demo2.demo.repository.UserRepository;
import com.demo2.demo.service.UserService;
import com.demo2.demo.service.base.impl.BaseServiceImpl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
public class UserServiceImpl extends BaseServiceImpl<UserModel, UUID> implements UserService {
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        super(userRepository, UserModel.class);
        this.userRepository = userRepository;
    }

    @Override
    public UserModel update(UserModel user, UUID id) {
        UserModel updateUser = this.userRepository.findById(id)
                .orElse(null);
        if (updateUser == null) {
            log.error("User with id: " + id.toString() + " not found");
        }
        updateUser.setEmail(user.getEmail());
        updateUser.setUsername(user.getUsername());
        return this.userRepository.save(updateUser);
    }
}
