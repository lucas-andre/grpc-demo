package com.demo2.demo.service;

import com.demo2.demo.model.domain.UserModel;
import com.demo2.demo.service.base.BaseService;

import java.util.UUID;

public interface UserService extends BaseService<UserModel, UUID> {
    UserModel update(UserModel user, UUID uuid);
}
