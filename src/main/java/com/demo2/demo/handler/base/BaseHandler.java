package com.demo2.demo.handler.base;

import com.demo2.demo.model.base.BaseIdModel;
import com.demo2.demo.service.base.BaseService;
import com.inker.grpc.GetCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Slice;

import java.util.UUID;

@Slf4j
public class BaseHandler<T extends BaseIdModel, ID extends UUID> {
    private final BaseService<T, ID> tidBaseService;
    private final Class<T> type;

    // TODO: CREATE REAL BASE HANDLER

    public BaseHandler(BaseService<T, ID> tidBaseService, Class<T> type) {
        this.tidBaseService = tidBaseService;
        this.type = type;
    }

    public T create(T requestFromProto) {
        return this.tidBaseService.create(requestFromProto);
    }

//    public Slice<T> findAll(GetCommand command) {
//        return this.tidBaseService.findAll(command);
//    }

    public T update() {
        return null;
    }

    public void remove() {

    }

}
