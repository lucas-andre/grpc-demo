package com.demo2.demo.handler;

import com.demo2.demo.handler.base.BaseHandler;
import com.demo2.demo.model.domain.UserModel;
import com.demo2.demo.service.UserService;
import com.inker.grpc.*;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class UserHandler extends BaseHandler<UserModel, UUID> {
    private final UserService userService;


    // TODO: CAMBIAR LOS TO_PROTO Y FROM_PROTO POR BeansUtils Y HACERLO EN EL SERVICIO

    public UserHandler(UserService userService) {
        super(userService, UserModel.class);
        this.userService = userService;
    }

    public CreateUserResponse create(UserCreateRequest userCreateRequest) {
        return this.userService.create(
                UserModel.fromProto(userCreateRequest)
        ).toCreateProto();
    }

    public Slice<GetUserResponse> findAll(GetCommand command) {
        return this.userService.findAll(command).map(UserModel::toGetProto);
    }

    public UpdateUserResponse update(UserUpdateRequest userUpdateRequest) {
        return this.userService.update(
                UserModel.fromProto(userUpdateRequest),
                UUID.fromString(userUpdateRequest.getId())
        ).toUpdateProto();
    }

    public DeleteResponse remove(Id id) {
        return this.userService.remove(
                UUID.fromString(id.getId())
        ).toDeleteProto();
    }
}
