package com.demo2.demo.repository;

import com.demo2.demo.model.domain.UserModel;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UserRepository extends CassandraRepository<UserModel, UUID> {
    UserModel findByUsername(String username);
}
