package com.demo2.demo.grpc;

import com.demo2.demo.handler.UserHandler;
import com.inker.grpc.*;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
@GRpcService
public class UserGrpc extends UserServiceGrpc.UserServiceImplBase {
    @Autowired
    private UserHandler userHandler;

    @Override
    public void create(UserCreateRequest request, StreamObserver<CreateUserResponse> responseObserver) {
        log.info("Starting creating User ...");
        try {
            CreateUserResponse response = this.userHandler.create(request);
            responseObserver.onNext(response);
            responseObserver.onCompleted();
        } catch (Exception e) {
            e.printStackTrace();
            responseObserver.onError(new StatusException(Status.ABORTED.withDescription("Handler conflict to save User")));
        }
        log.info("Finished creating User");
    }

    @Override
    public void getUsers(GetCommand request, StreamObserver<GetUserResponse> responseObserver) {
        log.info("Starting get User ...");
        System.out.println("request " + request );
        try {
            this.userHandler.findAll(request).forEach(user -> responseObserver.onNext(user));
            responseObserver.onCompleted();

        } catch (Exception e) {
            e.printStackTrace();
            responseObserver.onError(new StatusException(Status.ABORTED.withDescription("Handler conflict to find Users")));
        }
        log.info("Finished get User");
    }

    @Override
    public void updateUser(UserUpdateRequest request, StreamObserver<UpdateUserResponse> responseObserver) {
        log.info("Starting update User ...");
        try {
            UpdateUserResponse userResponse = this.userHandler.update(request);
            responseObserver.onNext(userResponse);
            responseObserver.onCompleted();
        } catch (Exception e) {
            e.printStackTrace();
            responseObserver.onError(new StatusException(Status.ABORTED.withDescription("Handler conflict to update Users")));
        }
        log.info("Finished update User");
    }

    @Override
    public void deleteUser(Id request, StreamObserver<DeleteResponse> responseObserver) {
        log.info("Starting delete User ...");
        try {
            DeleteResponse deleteResponse = this.userHandler.remove(request);
            responseObserver.onNext(deleteResponse);
            responseObserver.onCompleted();
        } catch (Exception e) {
            e.printStackTrace();
            responseObserver.onError(new StatusException(Status.ABORTED.withDescription("Handler conflict to delete Users")));
        }
        log.info("Finished delete User");
    }
}

