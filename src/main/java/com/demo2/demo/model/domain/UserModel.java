package com.demo2.demo.model.domain;

import com.demo2.demo.model.base.BaseIdModel;
import com.inker.grpc.*;
import lombok.*;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.Indexed;
import org.springframework.data.cassandra.core.mapping.Table;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table("user")
public class UserModel extends BaseIdModel {
    @Indexed
    private String username;

    @Column("email")
    private String email;
    @Column("password")
    private String password;
    @Column("type")
    private String type;

    @Column("first_name")
    private String firstName;
    @Column("last_name")
    private String lastName;

    public static UserModel fromProto(UserCreateRequest proto) {
        UserModel entry = new UserModel();
        entry.setUsername(proto.getUser().getUsername());
        entry.setEmail(proto.getUser().getEmail());
        entry.setPassword(proto.getUser().getPassword());
        return entry;
    }

    public static UserModel fromProto(UserUpdateRequest proto) {
        UserModel entry = new UserModel();
        entry.setUsername(proto.getUser().getUsername());
        entry.setEmail(proto.getUser().getEmail());
        return entry;
    }

    public CreateUserResponse toCreateProto() {
        return CreateUserResponse.newBuilder()
                .setId(getId().toString())
                .setUsername(getUsername())
                .setEmail(getEmail())
                .build();
    }

    public UpdateUserResponse toUpdateProto() {
        return UpdateUserResponse.newBuilder()
                .setId(getId().toString())
                .setUsername(getUsername())
                .setEmail(getEmail())
                .build();
    }

    public GetUserResponse toGetProto() {
        return GetUserResponse.newBuilder()
                .setId(getId().toString())
                .setUsername(getUsername())
                .setEmail(getEmail())
                .build();
    }

    public DeleteResponse toDeleteProto() {
        return DeleteResponse.newBuilder()
                .setId(getId().toString())
                .setUsername(getUsername())
                .build();
    }
}
